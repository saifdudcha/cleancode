# ক্লিন কোড/ ক্লিয়ার কোড

---


## ১. ভেরিয়েবল এবং মেথডের নাম

---

### ১.১. চিন্তার সাথে সংগতিপূর্ণ নাম ( নামের ক্ষেত্রে ক্যামেলকেসের ব্যবহার )


এড়িয়ে যাওয়া ভালো :

protected $d; // elapsed time in days


এগুলো উত্তম:

protected $elapsedTimeInDays;

protected $daysSinceCreation;

protected $daysSinceModification;

protected $fileAgeInDays;


### ১.২.উচ্চারণযোগ্য নামের ব্যবহার


এড়িয়ে যাওয়া ভালো :

public $genymdhms;

public $modymdhms;


এগুলো উত্তম:

public $generationTimestamp;

public $modificationTimestamp;


### ১.৩. প্রিফিক্স নাম পরিহার করুন

Most modern programming languages (including PHP and Python) support namespaces.


এড়িয়ে যাওয়া ভালো :

class Part {

private $m_dsc;

}


এগুলো উত্তম:

class Part {

private $description;

}


### ১.৪. কাব্যিক শব্দ পরিহার করুন


এড়িয়ে যাওয়া ভালো :

$program->whack();


এগুলো উত্তম:

$program->kill();


### ১.৫. প্রতিটি কনসেপ্টের জন্য একটি শব্দ ব্যবহার

get and fetch বিভিন্ন ক্লাসে একই জিনসি ব্যবহার করতে 


### ১.৬.  solution domain names এর ব্যবহার
solution domain terminology যেন অন্য প্রোগ্রামারগণ বুঝতে পারে 
 
উদাহরণস্বরূপ: 

jobs এর চেয়ে jobQueue বলা ভালো


### ১.৭.  function  এর নামের ক্ষেত্রে ক্রিয়াপদ এবং class ও attributes এর ক্ষেত্রে noun এর ব্যবহার

class Product {

private $price;

public function increasePrice($dollarsToAddToPrice)

{

$this->price += $dollarsToAddToPrice;

}

}



## ২. উত্তম ফাংশন

---

### ২.১. ছোট ফংশন ভালো

### ২.২. একটি ফাংশন দ্বারা একটি কাজের নির্দেশনা দেওয়া উচিত 

### ২.৩.  nested control structure  এড়িয়ে চলা ভালো

### ২.৪. arguments ছোট হওয়াই ভালো

( তিনটির অধিক আর্গুমেন্ট শয়তানের সমতুল্য

উদাহরণস্বরূপ:

Circle makeCircle(Point center, double radius);

ওপরেরটি নিচেরটি থেকে ভালো

Circle makeCircle(double x, double y, double radius);

### ২.৫. side effects না থাকা বাঞ্চনীয়

ফাংশন অবশ্যই যে নামে হবে তাই নির্দেশ করবে অন্য কিছু নয়।

### ২.৬. output arguments এড়িয়ে চলা ভালো

উদাহরণস্বরূপ:

email.addSignature();

ওপরেরটি নিচেরটি থেকে ভালো

addSignature(email);

### ২.৭. Error Handling একটি বিষয় (চিজ)

উপযুক্ত ও ব্যতিক্রম কোড দিয়ে  Error Handling  করা যেতে পারে

এখানে ‘Asking for forgivenes’ ‘requesting permission’-এর থেকে ভালো

 ‘conditions if possible’ -এর বিপরীতে try/catch ব্যবহার করা ভালো

### ২.৮.  বারবার একই জিনিস না বলাই ভালো

Function অবশ্যই মজবুত হবে



## ৩. Comments

---

### ৩.১. Don’t comment bad code, rewrite it

### ৩.২. If code is readable you don’t need comments

This is bad:

// Check to see if the employee is eligible for full benefits

if ($employee->flags && self::HOURLY_FLAG && $employee->age > 65)

This is good:

if ($employee->isEligibleForFullBenefits())

### ৩.৩. Explain your intention in comments

উদাহরণস্বরূপ:

// if we sort the array here the logic becomes simpler in calculatePayment() method

### ৩.৪. Warn of consequences in comments

উদাহরণস্বরূপ:

// this script will take a very long time to run

### ৩.৫.  Emphasis important points in comments

উদাহরণস্বরূপ:

// the trim function is very important, in most cases the username has a trailing space

### ৩.৬.  Always have your PHPDoc comments
Most IDEs do this automatically, just select the shortcut.

Having doc comments are especially important in PHP because methods don’t have argument and return types. Having doc comments lets us specify argument and return types for functions.

If your function name is clear, you don’t need a description of what the method is doing in the doc comment.

উদাহরণস্বরূপ:

/**
* Return a customer based on id
* @param int $id
* @return Customer
*/
Public function getCustomerById($id)

### ৩.৭.  এক কমেন্টের উপর আরেকটি কমেন্ট করা ভালো না

### ৩.৮.  অযথা কমেন্ট করা উচিত নয়

উদাহরণস্বরূপ:

/** The day of the month. */
private $dayOfMonth;

### 3.9. Never leave code commented
Perhaps this is the most important point in this whole article. With modern source control software such as Git you can always investigate and revert back to historical code. It is ok to comment code when debugging or programming, but you should never ever check in commented code.



## ৪. Other code smells and heuristics

---

There are a lot more that you can do to identify and avoid bad code. Below is a list of some code smells and anti-patterns to avoid. Refer to the sources of this blog for more information.

Dead code

Speculative Generality - no need “what if?”

Large classes

God object

Multiple languages in one file

Framework core modifications

Overuse of static (especially when coding in Joomla!)

Magic numbers - replace with const or var

Long if conditions - replace with function

Call super’s overwritten methods

Circular dependency

Circular references

Sequential coupling

Hard-coding

Too much inheritance - composition is better than inheritance